#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Promotion engine
# -----------------------------------------------------------------------------
# author:   Douglas Bezerra Beniz
# email:    douglasbebeniz@gmail.com
# -----------------------------------------------------------------------------

import pytest
import promo_engine

from promo_engine import PromoEngine

# -----------------------------------------------------------------------------
# Unit tests
# -----------------------------------------------------------------------------
def test_unit_product_a():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_product('A', 50)

    assert objPromo.product_prices.get('A') == 50

def test_unit_product_b():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_product('B', 30)

    assert objPromo.product_prices.get('B') == 30

def test_unit_product_c():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_product('C', 20)

    assert objPromo.product_prices.get('C') == 20

def test_unit_product_d():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_product('D', 15)

    assert objPromo.product_prices.get('D') == 15

def test_unit_promotion_1():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_promotion('P1', ['A'], [3], 130)

    assert objPromo.promotions.get('P1')[0][0]  == 'A'
    assert objPromo.promotions.get('P1')[1][0]  == 3
    assert objPromo.promotions.get('P1')[2]     == 130

def test_unit_promotion_2():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_promotion('P2', ['B'], [2], 45)

    assert objPromo.promotions.get('P2')[0][0]  == 'B'
    assert objPromo.promotions.get('P2')[1][0]  == 2
    assert objPromo.promotions.get('P2')[2]     == 45

def test_unit_promotion_3():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_promotion('P3', ['C','D'], [1,1], 30)

    assert objPromo.promotions.get('P3')[0][0]  == 'C'
    assert objPromo.promotions.get('P3')[0][1]  == 'D'
    assert objPromo.promotions.get('P3')[1][0]  == 1
    assert objPromo.promotions.get('P3')[1][1]  == 1
    assert objPromo.promotions.get('P3')[2]     == 30

def test_unit_order_1():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_order('O1', {'A':1,'B':1,'C':1})

    assert objPromo.orders.get('O1').get('A') == 1
    assert objPromo.orders.get('O1').get('B') == 1
    assert objPromo.orders.get('O1').get('C') == 1

def test_unit_order_2():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_order('O2', {'A':5,'B':5,'C':1})

    assert objPromo.orders.get('O2').get('A') == 5
    assert objPromo.orders.get('O2').get('B') == 5
    assert objPromo.orders.get('O2').get('C') == 1

def test_unit_order_3():
    # instance of the class
    objPromo = PromoEngine()
    objPromo.add_order('O3', {'A':3,'B':5,'C':1,'D':1})

    assert objPromo.orders.get('O3').get('A') == 3
    assert objPromo.orders.get('O3').get('B') == 5
    assert objPromo.orders.get('O3').get('C') == 1
    assert objPromo.orders.get('O3').get('D') == 1

# -----------------------------------------------------------------------------
# Integration tests
# -----------------------------------------------------------------------------

# Arrange necessary objects
@pytest.fixture
def promoEngineObj():
    # instance of the class
    objPromo = PromoEngine()
 
    # adding products and individual prices
    objPromo.add_product('A', 50.0)
    objPromo.add_product('B', 30.0)
    objPromo.add_product('C', 20.0)
    objPromo.add_product('D', 15.0)

    # adding promotions
    objPromo.add_promotion('P1', ['A'], [3], 130)
    objPromo.add_promotion('P2', ['B'], [2], 45)
    objPromo.add_promotion('P3', ['C','D'], [1,1], 30)
    objPromo.add_promotion('P4', ['C'], [5], '60%C')

    # adding orders
    objPromo.add_order('O1', {'A':1,'B':1,'C':1})
    objPromo.add_order('O2', {'A':5,'B':5,'C':1})
    objPromo.add_order('O3', {'A':3,'B':5,'C':1,'D':1})
    objPromo.add_order('O4', {'A':5,'B':5,'C':5})

    return objPromo

def test_scenario_a(promoEngineObj):
    assert promoEngineObj.total_amount('O1') == 100

def test_scenario_b(promoEngineObj):
    assert promoEngineObj.total_amount('O2') == 370

def test_scenario_c(promoEngineObj):
    assert promoEngineObj.total_amount('O3') == 280

def test_scenario_d(promoEngineObj):
    assert promoEngineObj.total_amount('O4') == 410
