#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Promotion engine
# -----------------------------------------------------------------------------
# author:   Douglas Bezerra Beniz
# email:    douglasbebeniz@gmail.com
# -----------------------------------------------------------------------------

class PromoEngine:
    def __init__(self):
        """
        Dictionaries, working as temporary 'databases'
        """
        # format of product_prices:
        #   { SKU_ID-1: PRICE-1,
        #     ..,
        #     SKU_ID-n: PRICE-n }
        self.product_prices  = {}

        # format of promotions:
        #   { PROMO_ID-1: [ [SKU_ID-1, ..,SKU_ID-n], [N_ITEMS-1, .., N_ITEMS-n], PRICE_PROMO-1 ],
        #     ..,
        #     PROMO_ID-n: [ [SKU_ID-1, ..,SKU_ID-n], [N_ITEMS-1, .., N_ITEMS-n], PRICE_PROMO-n ] }
        self.promotions      = {}

        # format of orders:
        #   { ORDER_ID-1: { SKU_ID-1: N_ITEMS-1, .., SKU_ID-n: N_ITEMS-n },
        #     ..,
        #     ORDER_ID-n: { SKU_ID-1: N_ITEMS-1, .., SKU_ID-n: N_ITEMS-n } }
        self.orders          = {}

    """
    Fill in 'database' of products and their prices
    """
    def add_product(self, description, price):
        self.product_prices[description.upper()] = price

    """
    Fill in 'database' of promotions
    """
    def add_promotion(self, description, prod_list, items_list, promo_price):
        self.promotions[description.upper()] = [prod_list, items_list, promo_price]

    """
    Fill in 'database' of orders
    """
    def add_order(self, description, order_products):
        self.orders[description.upper()] = order_products

    """
    Calculates total amount to be paid for products considering existent
      promotions
    """
    def total_amount(self, order_desc):
        try:
            # local variables
            __total = 0.0

            # ---------------------------------------------------------------------
            # make a copy of products and quantities list to be used in
            #   calculations
            # ---------------------------------------------------------------------
            __tmp_order = self.orders.get(order_desc)

            # ---------------------------------------------------------------------
            # iterate through all promotions, checking if any of them
            #   are applicable in this order
            # ---------------------------------------------------------------------
            for __promo_desc in self.promotions:
                __promo = self.promotions.get(__promo_desc)
                if len(__promo[0]) != len(__promo[1]):
                    # inconsistent promotion, ignore it
                    continue

                # verify if this promotion applies for the considered order
                if all(prod in __tmp_order for prod in __promo[0]):
                    __cnt_promos    = [0] * len(__promo[0])
                    __promo_price   = 0.0
                    if type(__promo[2]) == str and '%' in __promo[2]:
                        __promo_percent = __promo[2].split('%')
                        __promo_price = (float(__promo_percent[0])/100) * self.product_prices.get(__promo_percent[1]) * __promo[1][0]
                    else:
                        __promo_price = __promo[2]

                    for __idx, (__prod_promo, __n_item_promo) in enumerate(zip(__promo[0], __promo[1])):
                        # obtain the number of items in the promotion under analysis
                        __order_items = __tmp_order.get(__prod_promo)
                        while __order_items >= __n_item_promo:
                            # increment counter
                            __cnt_promos[__idx] += 1
                            # update the temporary order list
                            __order_items = __order_items - __n_item_promo
                            __tmp_order[__prod_promo] = __order_items
                    if min(__cnt_promos) == max(__cnt_promos):
                        # accummulate promotional price
                        __total += __cnt_promos[0] * __promo_price
                    else:
                        # not all combined products in the promotion were identified,
                        #   should restore ordered items
                        for __idx,__count in enumerate(__cnt_promos):
                            __tmp_order[__promo[0][__idx]] = __tmp_order.get(__promo[0][__idx]) + (__cnt_promos[__idx] - min(__cnt_promos))
                        # then apply the promotion, if it was identified at least
                        #   once, i.e. min(counter) > 0
                        __total += min(__cnt_promos) * __promo_price

            # ---------------------------------------------------------------------
            # at the end takes all remaining items calculating normal prices
            # ---------------------------------------------------------------------
            for prod_desc in __tmp_order:
                __total += __tmp_order.get(prod_desc) * self.product_prices.get(prod_desc)

            return __total
        except Exception as e:
            raise e

"""
Procedure called when the script starts
"""
def main():
    objPromo = PromoEngine()

    # adding products and individual prices
    objPromo.add_product('A', 50.0)
    objPromo.add_product('B', 30.0)
    objPromo.add_product('C', 20.0)
    objPromo.add_product('D', 15.0)
    print(objPromo.product_prices)

    # adding promotions
    objPromo.add_promotion('P1', ['A'], [3], 130)
    objPromo.add_promotion('P2', ['B'], [2], 45)
    objPromo.add_promotion('P3', ['C','D'], [1,1], 30)
    objPromo.add_promotion('P4', ['C'], [5], '60%C')
    print(objPromo.promotions)

    # adding orders
    objPromo.add_order('O1', {'A':1,'B':1,'C':1})
    objPromo.add_order('O2', {'A':5,'B':5,'C':1})
    objPromo.add_order('O3', {'A':3,'B':5,'C':1,'D':1})
    objPromo.add_order('O4', {'A':5,'B':5,'C':5})
    print(objPromo.orders)

    for o in objPromo.orders:
        print("order: %s with total: %.2f" % (o, objPromo.total_amount(o)))


if __name__ == '__main__':
    main()

